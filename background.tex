\begin{figure*}[t]
\centering
\includegraphics[scale=.27]{ssd-internal}
\vspace{-5pt}
\caption{SSD internals: from the host interface to the NAND cell.}
\label{ssd-internal}
\vspace{-5pt}
\end{figure*}

\subsection{SSD and Flash Organization}
%We focus on the necessary information and show it abstractly to better understand our claim, although an actual SSD includes complex details and other important components.
Figure \ref{ssd-internal} depicts the SSD internals, increasing gradually in detail by touching only the parts involved in this work.

\noindent \emph{\bf SSD internals:~}
An SSD communicates with its host system via host interface logic (such as SATA \cite{ref-if-sata} and PCIe \cite{ref-if-pcie3}) where I/O requests and their data are delivered. Small computing resources with temporal buffers take a main role to run flash software. Among a variety of flash software functions, we emphasize two specific functions, \emph{page allocation} and \emph{page mapping}, used to provide the program data with a new flash space and to track the data location, respectively (Section \ref{sec:io2nand} describes these software modules in detail). The underlying medium consists of multiple flash chips which are clustered by separate buses and controllers.


\noindent \emph{\bf Flash internals:~}
Each flash chip is divided into two parts: the memory cell array and the control part. The memory cell array is grouped into smaller units: plane, block, and page. A plane comprising blocks has its own data register where user data is temporarily kept when programming it to a block within the plane. A block is a set of \emph{pages} which is the basic unit of flash read and program. On the other hand, the I/O logic controls pinout to communicate with the outside, whereas the control logic decodes a NAND command to determine the target page, and activates the necessary components to execute the command. 
%In general, a plane contains over tens of thousands blocks, while each block involves around a few hundred pages, although the numbers can vary depending on the design requirement.


\noindent \emph{\bf Circuit details:~} A memory cell is a floating gate transistor (FGT) which can express one (SLC) or more logical bits (MLC and TLC) by capturing the designated amount of electrons therein (in this paper, we define the cell state as ``0", if the corresponding FGT holds the electrons). Groups of cells are joined together to from strings whose ends are connected with the bit line (BL) and source line (SL), respectively. In the connection between BL/SL and a string, the string select transistor (SST) and the ground select transistor (GST) are placed, which are controlled by the string select line (SSL) and the ground select line (GSL), respectively. Orthogonally, a shared metal, called word line (WL), is connected to the control gates of FGTs across strings. The number of FGTs connected to a single WL constitutes the number of bits in a page  (e.g., 32768 FGTs for a 4KB-page), which is determined at design time.

% description about electrons captured by FGT
% 0 (programmed state) & % 1(erased state)


\begin{figure}
\centering
\includegraphics[width=1\linewidth]{misalignment}
\vspace{-10pt}
\caption{Two misaligned I/O examples which amplify the actual written data volume.}
\label{fig:misalign-example}
\vspace{-15pt}
\end{figure}




\subsection{From an I/O Request to NAND Commands}
\label{sec:io2nand}
The I/O format (and its address space) that the host file system employs is different from the NAND command format to operate flash memory. The address translation from an I/O request to NAND commands involves two steps: \emph{to-LPN} conversion and \emph{to-PPN} conversion.

\noindent \emph{\bf I/O misalignment in to-LPN conversion:~} The difference in the working unit sizes between the host system (sector) and flash memory (page) requires the \emph{to-LPN} conversion to access the underlying flash medium. When the sector address of an I/O request is converted to the logical page number (LPN) for NAND command(s), a misaligned address or size increases the actual write data size. Figure \ref{fig:misalign-example} provides two misaligned I/O examples that waste flash space by appending dummy data. The start sector of W1 falls in the middle of the first LPN, while the data size of W1 fits to a (flash) page size, which results in \emph{address misalignment}. Since the I/O address range lies across two logical pages, two different NAND commands which include dummy data are issued to serve this I/O request. Consequently, the size of the original user data doubles, which wastes the flash space. In comparison, the size of W2 is not a multiple of the page size, and the start sector of W2 matches with the logical page, which leads to \emph{size misalignment}. To serve W2 data which is one and half times of the page size, two NAND commands, of which the second one consists of half user data and half dummy data, are needed. In this way, the size misalignment also brings unnecessary flash space consumption. 

\noindent \emph{\bf Flash software involved in to-PPN conversion:~} The following \emph{to-PPN} conversion maps the LPN to the physical page number (PPN) where the logical page is actually stored (note that a logical page can be placed into any physical page of the flash memory). To realize the \emph{to-PPN} conversion, two flash software mechanisms, \emph{page allocation} and \emph{page mapping}, are necessary. The page allocation function selects an appropriate physical page to serve the current program command, whereas the mapping information between the LPN and the PPN (allocated by the page allocation) is maintained by the page mapping function. Through the above conversions, a host I/O request of the form (start address, sector size) is transformed into one or more NAND commands, in the form of (chip\#, block\#, page\#), to specify the target page. Once the target chip gets the NAND command, its control logic interprets the command and operates the corresponding circuits.


\begin{table}
\scriptsize
\begin{tabular}{|p{24pt} ||p{25pt} | p{25pt} | p{10pt} | p{20pt} | p{30pt} | p{12pt} | p{7pt} |}
\hline
Circuit & $WL_{pgm}$ & $WL_{pass}$ & $SSL$ & $BL_{pgm}$ & $BL_{inhibit}$ & $GSL$ & $SL$\\
\hline
Voltage & $V_{pgm}$ & $V_{pass}$ & $V_{dd} $ &  0 & $V_{dd}$  & 0 & 0 \\
\hline
\end{tabular}
\vspace{-5pt}
\caption{Voltage values used for programming a page.}
\vspace{-15pt}
\label{tab:voltage}
\end{table}




\subsection{A Page Program Mechanism}
Programming a flash page is carried out in two steps: the data movement and the memory cell array program. Before accessing the flash memory cells, the user data to be written has to be loaded into the data register of the chip via the I/O logic.

Once the user data is ready in the data register, the control logic programs it into the target page, which is physically a set of FGTs attached to the same WL. To implement this, the control circuits are given the necessary voltage values, as shown in Table \ref{tab:voltage}. $WL_{pgm}$ is the WL connecting FGTs in the target page to be programmed, whereas $WL_{pass}$ stands for all the other WLs in the block. Compared to $V_{pass}$ applied to $WL_{pass}$s (for other pages), $V_{pgm}$ of $WL_{pgm}$ is much higher, and is used to pull electrons into the floating gate. On the other hand, within the target page, some FGTs may want to be programmed, while the others need to remain erased (not programmed), depending on the user data values, which is controlled by BL. $BL_{pgm}$ is set to ground for the FGTs that are programmed, while $V_{dd}$ is applied to $BL_{inhibit}$ for the FGTs that are not programmed. To sum up, the target page is selected by WL voltage among all pages in the block, and the FGTs to be programmed within the target page are identified using BL voltages.  


\begin{figure}
\centering
\includegraphics[scale=.23]{motive}
\vspace{-5pt}
\caption{An example data corruption caused by intra-page program disturb.}
\vspace{-5pt}
\label{motive}
\vspace{-15pt}
\end{figure}


\subsection{Motivation: Program Disturb in a Page}
\emph{Program disturb} (neighboring cell values are unintentionally changed by a memory cell program) is one of the major flash error contributors \cite{program-distrub-3}\cite{yucai-2}\cite{disturb-1}\cite{program-distrub-2}\cite{yucai-3}, which comes in two varieties: \emph{inter-page disturb} and \emph{intra-page disturb}. In this subsection, we focus on intra-page program disturbance to show what happens when the same page is programmed multiple times in attempt to reuse the dummy space generated by the I/O misalignment. The inter-page disturb is discussed later in Section \ref{sec:archi-io}.

Figure \ref{motive} illustrates an example of two consecutive programs to the same 4bit-page, which consists of a $WL_{N}$ and four $BLs$. The first program intends to write the first two bits of the data register, whereas the second one is for writing the last two bits of the data register (since there is no erase between the two consecutive programs, the first 2 bits of data is expected to be still valid even after the second program). During the first program (the upper figure), a very high voltage ($V_{pgm}$) is applied to $WL_{N}$, while the voltage values for the first two BLs depend on the first two data values of the data register. The first bit of the data is a logical value `0' and the corresponding BL becomes $BL_{pgm}$, while the second BL is $BL_{inhibit}$ due to the logical value `1'. The last two BLs (whose FGTs want to remain without any change) are also $BL_{inhibit}$, where $V_{vdd}$ is applied. It is to be noted that the second cell (FGT) remains the same without holding electrons as a result of the first program. Subsequently, the second program targets the same page; however, the last two bits are intended to program this time. In contrast with the first program, the voltage values applied to the last two BLs depend on the data values of the data register, while the first two BLs are treated as $BL_{inhibit}$ since the first two bit values have to be preserved. Now, a very high voltage ($V_{pgm}$) is applied to the WL again to allow the target FGTs to capture electrons. Unfortunately, due to this very high voltage, the second cell holding the logical value `1' (which should remain the same after the second program) can be distorted to the logical value `0' by unintentionally capturing the electrons, which is the \emph{intra-page program disturb}.

The intra-page program disturb is caused by the fact that all FGTs comprising a physical page share one WL. This sharing of WL across the whole page brings repeated high voltage application to the previously-used data portion in a page when other page locations are being programmed. As shown in the above example, the empty FGTs have a tendency to capture electrons, which change their logical value from `1' to `0'. This is why state-of-the-art flash memories (i.e., MLC/TLC flash based on very low technology nodes) prohibit repeated programs to the same page by limiting NOP to only one. In current architectures, the reuse of dummy place in a page (caused by I/O misalignment) is not allowed in order to preserve data. Motivated by this, we propose a new flash architecture that can allow multiple programs to the same page by dividing the shared WL in the original architecture into multiple WLs.





















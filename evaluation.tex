\begin{table}
\scriptsize
\vspace{-5pt}
\begin{tabular}{|p{40pt} | p{22pt}  | p{22pt} | p{38pt}  | p{35pt}  | p{25pt}  | }
\hline
\textbf{Workload} & \textbf{Write ratio (\%)} & \textbf{Avg. write size (KB)} & \textbf{Mis-align-ment @4KB page(\%)} & \textbf{Address misalignment (\%)} & \textbf{Size misalignment (\%)} \\
\hline \hline
cfs & 24.9 & 12.5 & 38.8 & 0 & 100 \\
\hline
fin & 76.8 & 3.7 & 99.8 & 89.1 & 10.9 \\
\hline
hm & 64.5 & 8.3 & 53.6 & 37 & 63 \\
\hline
mds & 7.1 & 13.8 & 97.5 & 98 & 2 \\
\hline
msnfs & 23.4 & 10.4  & 99.9 & 83.3 & 16.7 \\
\hline
prxy & 96.9 & 4.6 & 92.4 & 68.9 & 31.1 \\
\hline
prn & 98.1 & 6.1 & 75.6 & 64.8 & 35.2 \\
\hline
proj & 90.1 & 31.1 & 100 & 100 & 0 \\
\hline
usr & 91.9 & 8.7 & 51.6 & 25.4 & 74.6 \\
\hline
hpio & 100 & 6.5 & 100 & 100 & 0 \\
\hline
\end{tabular}
\caption{Characteristics of our workloads.}
\label{tab:workload-cha}
\vspace{-15pt}
\end{table}

%The misalignment in both address and size are categorized as the address misalignment.

\subsection{Setup}
\label{eval-setup}
 
\noindent \emph{\bf Simulation Framework:~} To evaluate PageSplit and PageSplit+, we modified the DiskSim simulator \cite{disksim} with SSD extensions \cite{ssd-ext}. We first implemented the I/O alignment module to generate aligned NAND program commands from the I/O traces. Then, we added both the sector-based programming (PageSplit) and the page allocation algorithms (PageSplit+) atop of the I/O misalignment-aware framework.

\noindent \emph{\bf System configurations:~} To mimic a modern SSD, we configured a 128GB SSD based on sixteen MLC flash chips \cite{eval-conf} as our baseline. Each flash chip has eight planes where each includes 2048 blocks. Each block consists of 128 4KB-pages. We fixed other timing parameters of the flash memory, since the proposed architecture targets the lifespan improvement at the micro-architectural level. In contrast, the page size is highly configurable in our experiments, because the I/O misalignment is sensitive to it, as shown in Figure \ref{fig:misalign-page-sense}. We also assume that there is no waste in ECC capability by employing an ECC whose unit size is a sector.


\noindent \emph{\bf Flash software:~} Among different address mapping mechanisms \cite{address-map}, our evaluations employed \emph{page mapping}, which is adopted in SSD-extended DiskSim. For the page allocation, we extended it to support PageSplit and PageSplit+. All other software functions such as the garbage collection algorithm are fixed to focus on the impact of I/O misalignment.


\noindent \emph{\bf Workloads:~} To evaluate the effectiveness of the proposed architecture in addressing the I/O misalignment problem, we utilized a variety of workloads ranging from different enterprise applications to HPC traces. The nine enterprise I/O workloads we collected from \cite{ref-trace1} and \cite{ref-trace2} are corporate mail server (\emph{cfs}), online transaction processing (\emph{fin}), hardware monitor (\emph{hm}), media server (\emph{mds}), remote file storage server (\emph{msnfs}), web proxy (\emph{prxy}), print server (\emph{prn}), project directory service (\emph{proj}), and shared home directories (\emph{usr}). An HPC workload \emph{(hpio)} from \cite{ref-trace3} is also employed. Table \ref{tab:workload-cha} lists the important characteristics of our workloads. Note that write I/O requests (as opposed to read requests) consuming flash pages affect the SSD lifetime, which is the concern of this paper.





\subsection{Lifetime Extension}
We mimic the lifetime extension of an SSD by using a metric called \emph{total flash program size}. The smaller the total flash program size is, the longer the flash lives. Figure \ref{eval-write} compares the total flash program size, when the same amount of I/O requests are served under three different architectures, baseline, PageSplit, and PageSplit+. The values are \emph{normalized} to the data size of the total write I/O requests. The proposed architectures are expected to improve the SSD lifetime by reducing the normalized value to 1.0.

% Note that the purpose of our architectures is to prevent misaligned I/O requests from be amplifed the program size when it is written to flash pages.


Due to the misaligned I/O writes, in the conventional flash architecture (baseline), the total flash program size is magnified by an average of 38\%, which in turn shortens the limited SSD lifetime. Recall from Figure \ref{fig:ch-perf} that, the seriousness of the I/O misalignment problem depends on the workload characteristics (i.e., the I/O address and size). For example, \emph{cfs} minimally affects the SSD lifetime, whereas \emph{prxy} significantly wastes the flash space. In contrast, the disastrous program volume expansion is successfully suppressed by the PageSplit architecture (18\% on average). This demonstrates the success of sector-based programming in removing the dummy space from the flash memory. Particularly, workloads seriously suffering from the misaligned I/O requests (such as \emph{fin} and \emph{prxy}) enjoy more benefits. The new page allocation strategy (PageSplit+) is able to further reduce the total flash program size. The improvement it brings ranges between 1\% and 52\%, averaging on 22\%. By covering up even the dummy space which PageSplit could not fill, PageSplit+ successfully reduces the normalized program size value to near one. Therefore, we can conclude that both the proposed architecture and flash software are promising solutions to the lifetime problem emanating from I/O misalignment.

\begin{figure}
\centering
\vspace{-10pt}
\includegraphics[scale=.11]{eval-write}
\vspace{-5pt}
\caption{The total program size under the three architectures (normalized to the total I/O data size).}
\label{eval-write}
\vspace{-15pt}
\end{figure}



\begin{figure}
\centering
\includegraphics[scale=.11]{eval-gc}
\vspace{-5pt}
\caption{The GC overhead as a result of the reduction in the total flash program size.}
\label{eval-gc}
\vspace{-15pt}
\end{figure}



\subsection{GC Overhead}
%Garbage collection (GC) is an unavoidable costly SSD operation. 
Due to the data overwrite prohibition rule, SSDs treat an update of already-written data as a \emph{new write}, which gradually reduces the available flash space. Garbage Collection (GC) is triggered to obtain new flash space by clustering valid data together and erasing blocks, which consumes P/E cycles and shortens the SSD lifespan. Thus, the GC overhead can be another indicator of the SSD lifetime.

Figure \ref{eval-gc} plots the reduction in the GC overhead when employing our two proposed techniques. The values in this graph are normalized to the GC overhead generated by the misaligned I/O requests in the baseline architecture. PageSplit can save GC overhead by 32\% on average, which is further reduced by PageSplit+ (on average, 40\%). In the workloads where the flash program size saved by PageSplit is significant, the GC overhead is also remarkably reduced. For instance, the proposed architecture triggers the GC much less frequently than the baseline when running \emph{fin} and \emph{prxy}. This is because the largely decreased flash program size reduces the GC frequency. On the other hand, \emph{cfs} shows a small reduction in the GC overhead, as it has little gain in the flash program size. Since the flash program size and the GC overhead exhibit similar trends, we believe that the proposed flash architecture can enhance SSD lifetime by successfully addressing the I/O misalignment problem. 



\subsection{Sensitivity Analysis}
The flash page size significantly varies in commercial SSD products. A major trend is to increase the page size to achieve high throughput \cite{page-trend}. However, as shown in Figure \ref{fig:misalign-page-sense}, the I/O misalignment problem becomes even more problematic as the page size an SSD employs increases. Thus, the effectiveness of PageSplit and PageSplit+ needs to be evaluated under various page sizes. Figure \ref{eval-write-sense} shows the reduction in the flash program size brought by PageSplit and PageSplit+, with page sizes ranging from 4KB to 32KB. The values are \emph{normalized} to the baseline architecture. One can see from these results that the proposed architecture becomes more effective when employing SSDs with large pages. This is because a large page frequently generates a dummy space inside, which is effectively reused by other program data with the help of our sector-based programming. PageSplit+ also provides less page consumption as the page size increases, since it can find more opportunities to reuse the dummy space, compared to PageSplit. One point we want to emphasize is that, even \emph{cfs} in 4KB-page flash, where the I/O misalignment problem is not serious and the benefit coming form PageSplit is negligible, starts to gain benefits from the PageSplit architecture. We conducted the same page sensitivity test for the GC overhead as well, and plotted it in Figure \ref{eval-gc-sense}. Similar to the decrease in the flash program size, the PageSplit's capability to reduce the GC overhead improves as the page size increases. We also observe the following two trends: (i) PageSplit+ brings additional benefits over PageSplit and (ii) all workloads including \emph{cfs} experience some gains when using large pages.



\begin{figure}
\centering
\includegraphics[scale=.12]{eval-write-sense}
\vspace{-10pt}
\caption{The total program size written in the proposed flash architectures with varying page sizes.}
\label{eval-write-sense}
\vspace{-15pt}
\end{figure}






\subsection{Performance}
When processing a program, the data to be written moves in two different paths from the chip out to the internal memory cell. First, the data is loaded into the data register of the chip inside via the chip pinout. Since the circuits added in the proposed architecture are located across the memory cells, there is no performance loss or improvement in the first path. Once the user data is ready in the data register, all the bit values comprising a whole page are programmed by accessing the memory cells at the same time. Since the role of voltage lines and switches is to apply $V_{pgm}$ to only a part of the whole WL, the latencies between a sector program and an entire page program are similar. Hence, no performance change is expected in any of these paths. The read latency also remains the same, because the data movement path is the same as the program. We want to emphasize that the proposed architecture is mainly devised to enhance the SSD \emph{lifespan} by providing the sector granularity access unit.

%Furthermore, we anticipate that the SAL delay is also negligible, compared to the long data movement time.
%(note that all FGTs in a page are attached to a shared WL to which the high voltage is applied when programming).
\begin{figure}
\centering
\includegraphics[scale=.12]{eval-gc-sense}
\vspace{-10pt}
\caption{The GC overhead in the proposed architectures with varying page sizes.}
\label{eval-gc-sense}
\vspace{-15pt}
\end{figure}








\documentclass[pageno]{jpaper}

\usepackage[normalem]{ulem}

\begin{document}

\title{Guidelines for Submissions to MICRO 2014}
%\author{Emre Ozer & Thomas F. Wenisch}

\date{}
\maketitle

\thispagestyle{empty}

\begin{abstract}
This document serves as a sample for submissions to MICRO 2014.
We provide some guidelines that authors should follow when submitting papers to
the conference.
\end{abstract}

\section{Introduction}

This document provides the formatting instructions for submissions to the 47th
Annual IEEE/ACM International Symposium on Microarchitecture,
2014~\cite{micro47}. In an effort to respect the efforts of reviewers and in
the interest of fairness to all prospective authors, we request that all
submissions to MICRO-47 follow the formatting and submission rules detailed
below.  Submissions that (grossly) violate these instructions may not be
reviewed, at the discretion of the program chair, in order to maintain a review
process that is fair to all potential authors.

An example submission (formatted using the MICRO-47 submission format) that
contains the submission and formatting guidelines can be downloaded from here:
\href{http://www.microarch.org/micro47/files/micro47-template.pdf}{Sample
PDF}. The contents of this document are the same as the contents of the
submission instructions that appear on
\href{http://www.microarch.org/micro47/submission.html}{this website}.

All questions regarding paper formatting and submission should be directed to
the program chair.

\section{Preparation Instructions}


\subsection{New in 2014: 11+References format}

For MICRO-47, we are instituting a new paper submission format
and reference requirements. All submissions are allowed a maximum 
of 11 pages of single-spaced two-column text content according to
the formatting guidelines below.  In addition, submissions are 
allowed a references section of unlimited length outside of this
11-page limit.  

References must include complete author lists to facilitate
the reviewing process.	

\subsection{Paper Formatting} 

If you are using \LaTeX~to typeset your paper, then we suggest
that you use the template available here:
\href{http://www.microarch.org/micro47/files/micro47-latex-template.tar.gz}{\LaTeX~Template}.
(\href{http://www.microarch.org/micro47/files/micro47-template.pdf}{This
document} was prepared with that template.)  If you are using a different
software package to typeset your paper, then please adhere to the guidelines
mentioned in Table~\ref{table:formatting}.

\begin{table}[h!]
  \centering
  \begin{tabular}{|l|l|}
    \hline
    \textbf{Field} & \textbf{Value}\\
    \hline
    \hline
    Page limit & 11 pages + references\\
    \hline
    Paper size & US Letter 8.5in $\times$ 11in\\
    \hline
    Top margin & 1in\\
    \hline
    Bottom margin & 1in\\
    \hline
    Left margin & 0.75in\\
    \hline
    Right margin & 0.75in\\
    \hline
    Space between columns & 0.25in\\
    \hline
    Body font & 10pt\\
    \hline
    Abstract font & 10pt, italicized\\
    \hline
    Section heading font & 12pt, bold\\
    \hline
    Subsection heading font & 10pt, bold\\
    \hline
    Caption font & 9pt, bold\\
    \hline
    References & 8pt, no page limit, list all authors\\
    \hline
  \end{tabular}
  \caption{Formatting guidelines for submission.}
  \label{table:formatting}
\end{table}

\textbf{Please ensure that you include page numbers with your
submission}. This makes it easier for the reviewers to refer to
different parts of your paper when they provide comments.

\subsection{Content}

\noindent\textbf{\sout{Author List.}} All submissions are double
blind. Therefore, please do not include any author names in the
submission. You must also ensure that the metadata included in the
PDF does not give away the authors. If you are improving upon your
prior work, refer to your prior work as a third person and include
references to your past work. 

\noindent\textbf{Figures and Tables.} Ensure that the figures and
tables are legible.  Please also ensure that you refer to your
figures in the main text. Many reviewers print the papers in
gray-scale. Therefore, if you use colors for your figures, ensure
that the different colors are distinguishable in gray-scale.

\noindent\textbf{Main Body.} Avoid bad page or column breaks in
your main text, i.e., last line of a paragraph at the top of a
column or first line of a paragraph at the end of a column. If you
begin a new section or sub-section near the end of a column,
ensure that you have at least 2 lines of body text on the same
column. Note that the entire main body of your submission as well as any
Figures, footnotes, etc., must conform to the 11-page content limit;
only references may appear on additional pages.

\noindent\textbf{References.} There is no length limit for references. 
Each reference must explicitly list all authors of the paper. 
Papers not meeting this requirement will be rejected. Authors of NSF 
proposals should be familiar with this requirement. Knowing all
authors of related work will help find the best reviewers.

\section{Submission Instructions}

\subsection{Paper Authors}

Declare all the authors of the paper upfront. Addition/removal of authors once
the paper is accepted will have to be approved by the program chair.

\subsection{Conflict Responsibilities}

Authors must register all their conflicts on the paper submission site. 
Conflicts are needed to ensure appropriate assignment of reviewers. If a paper
is found to have an undeclared conflict that causes a problem OR if a paper 
is found to declare false conflicts in order to abuse or "game" the review 
system, the paper may be rejected. 
 
Please declare a conflict of interest (COI) with the following for any author of your paper:

\begin{enumerate}
\item Your Ph.D. advisor, post-doctoral advisor, and Ph.D. students
\item Other past or current advisors
\item Current or past students
\item Family relations by blood or marriage (if they might be potential reviewers)
\item People with the same affiliation
\item People whom you co-authored accepted/rejected/pending papers with in the last 5 years
\item People whom you co-authored accepted/pending grant proposals with in the last 5 years
\end{enumerate}

"Service" collaborations like co-authoring a CSTB report or co-presenting tutorials do 
not constitute conflicts. However, there may be others not covered 
by the above with whom you know a COI exists. Please report such COIs; however, you 
will need to justify them. Please be reasonable. For example, just because a reviewer
works on similar topics as your paper, you cannot declare a COI with that reviewer. We
will carefully check the justification of conflicts and take action when authors seem to be 
blacklisting reviewers without sufficient justification. 

We hope to draw most reviewers 
from the PC and the ERC, but others from the community may also write reviews. Please
declare all your conflicts (not just restricted to the PC and ERC). When in doubt, 
contact the program chair.


\subsection{Concurrent Submissions and Resubmissions of Already Published Papers}

By submitting a manuscript to MICRO-47, the authors guarantee that the
manuscript has not been previously published or accepted for
publication in a substantially similar form in any conference or
journal. The authors also guarantee that no paper which contains
significant overlap with the contributions of the submitted paper is
under review to any other conference or journal or workshop, or will
be submitted to one of them during the MICRO-47 review
period. Violation of any of these conditions will lead to rejection.

Extended versions of papers accepted to IEEE Computer Architecture
Letters can be submitted to MICRO-47.
As always, if you are in doubt, it is best to contact the program chair. 

\section{Submission Site}

A link to the submission site will be posted on the MICRO-47 web site
\href{http://www.microarch.org/micro47/}{http://www.microarch.org/micro47/}
in mid April. 

\bstctlcite{bstctl:etal, bstctl:nodash, bstctl:simpurl}
\bibliographystyle{IEEEtranS}
\bibliography{references}

\end{document}


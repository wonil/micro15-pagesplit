
\begin{figure}
\centering
\includegraphics[scale=.20]{ps-plus-io}
\vspace{-5pt}
\caption{Page allocation strategies employed by PageSplit and PageSplit+ with an eight-sector page.}
\label{ps-plus-io}
\vspace{-15pt}
\end{figure}


\subsection{Motivation: Limitations of PageSplit}
\label{sec:mot-psplus}
Even though PageSplit reduces the dummy space caused by the misaligned I/O requests, it can still miss many opportunities to eliminate the dummy space due to the combination of the in-page-order programming constraint, single block-based page allocation, and the size variance in the program data. A pre-defined program sequence in a block (to avoid the inter-page program disturb) prevents the dummy space in the lower-numbered pages from being revisited after the higher-numbered pages are used. This is why the default PageSplit gives up the dummy space in the active page when the program data does not fit into it. Unfortunately, program sizes (which are determined when the aligned program commands are constructed based on the I/O requests) can vary significantly, and a large fraction of them may not fit into the dummy space in the active page. Another point we want to emphasize is that there is no other remaining space in the other blocks to service the current program with the specific data size. In general, the flash software allocates a new page from a single active block until all the pages in that block are consumed. Once all the pages are spent out in the active block, a new active block is selected and, again, the pages in the block are allocated one by one. Hence, there can be considerable size mismatches between the remaining space and the program data, which reduces chances for fully utilizing the available flash space.


%This is because the continuous use of new blocks leads to the lack of clean space, which in turn quickly brings the garbage collection and shorten the lifetime. 

%If the currently-requested program size is bigger than the remaining space as a result of previous program(s), PageSplit has to give up the remaing space (which becomes dummy space) of the current page and allocate a new page to serve the given program.


An active block at the top left corner of Figure \ref{ps-plus-io} is managed by the default version of PageSplit (all the blocks shown assume a program page order which goes from the bottom to the top). A series of four program commands whose sizes (4, 5, 4, 5) are given to the flash and the pages of the active block are allocated to serve them. After the first program, whose size is 4, got its service in the first page, the remaining space size is 4 (out of 8). The next program size is 5 which is bigger than the remaining space (4), and cannot be served by the first page. Thus, the next new page is allocated to serve the second program by letting the remaining space of the first page be dummy. Unfortunately, the next (third) program needs a new page allocation, since it cannot be served by the remaining space of the second page (the remaining space is 3, whereas the program size is 4). In this way, the remaining space of the current page continuously fails to meet the program size during the service. In the worst case scenario, PageSplit cannot save any physical page by consuming a whole page to serve only one program command (even if its data size is smaller than the page size). Therefore, the default PageSplit may not be successful in reusing the dummy space due to (i) the limited dummy space candidates and (ii) the variance across program data sizes.



\SetAlFnt{\scriptsize\sf}
\DecMargin{0.5em}
\begin{algorithm}[t]
	\DontPrintSemicolon
	\SetAlgoVlined
	\KwIn{Cmd}
	\KwOut{(activeBlockNum, activePageNum, pageOffset)}
	\;
	baseBlkNum := $getBaseBlockNum$(Cmd) \;
	i := $getNumActiveBlocks()$ \;

	
	%/* try to find out space in other blocks */ \;
	\For { i > baseBlkNum } 
	{
		activePageNum	:= $getActivePageNum(i)$\;
		remainSpace :=  $getRemainSpace(activePageNum)$ \;
		\If { remainSpace < getMinServeSize(i) }
		{
			%/* this block has unavailable dummy space */ \;
			\If { Cmd.size <= remainSpace }
			{
				%/* the command size fits to the dummy space */ \;
				activeBlockNum = i \;
				pageOffset = PageSize - remainSpace \;
				return \;
			}
		}		

		i = i -1 \;
	}
	
	%/*  find out space in the base block */ \;
	\If {i = baseBlkNum } 
	{
		activePageNum	:= $getActivePageNum(baseBlkNum)$\;
		remainSpace :=  $getRemainSpace(baseBlkNum)$ \;
		\If { Cmd.size > remainSpace }
		{
			%/* new page is allocated */ \;
			activePageNum = activePageNum + 1 \;
			pageOffset = 0 \;
		}
		\Else
		{
			pageOffset = PageSize - remainSpace \; 
		}
	}
\caption{\footnotesize{The page allocation algorithm of PageSplit+, which picks up the most appropriate place from where the current program command can be served.}}
\label{alg:ps-plus}
\end{algorithm}



\subsection{A  New Page Allocation Strategy} 

To enhance the dummy space reduction capability of PageSplit, we present a new page allocation strategy, which is called PageSplit+. The underlying idea behind PageSplit+ is to secure ``multiple space candidates" to serve arbitrary size of program data. Among the multiple space candidates, the fittest space is selected to serve the program data.


\noindent \emph{\bf Multiple active blocks:~} Compared to default PageSplit with only one active block, PageSplit+ maintains multiple active blocks. \emph{Each active block is expected to serve the program commands with specific sizes}. Note that there is no need to retain as many active blocks as the number of every possible size (i.e., the number of sectors). For example, a block to serve data whose size is between the half of the page size and the page size would be useless, because two such  data cannot be programmed in a physical page, and the remaining portion becomes dummy space. Therefore, \emph{we limit the number of active blocks to the number of data size ``ranges"}. Figure \ref{ps-plus-io} illustrates an 8-sector page flash with three active blocks. Basically, each active block can serve only the designated program sizes; that is, active block 1 serves the program commands with size 1 or 2, whereas active block 2 provides a service for the program with size 3 or 4. The programs with all other sizes (from 5 to 8) are destined to active block 3. Therefore, compared to the single active block-based PageSplit, PageSplit+ can provide more places that can accommodate the current program size.

\noindent \emph{\bf Page allocation from multiple active blocks:~}
One potential drawback of the program command distribution over multiple active blocks is that the active blocks that serve programs with bigger data sizes (particularly, the one that serves the biggest data size range) can generate a lot of dummy space. For example, active block 3 of Figure \ref{ps-plus-io} can generate a dummy space in every page, when it serves programs with data sizes 5, 6, or 7. This is because only fixed sizes are allowed to serve in each active block.

To fill out the dummy space in the active block serving programs with large data, when serving a program with smaller data size, our new page allocation function first checks whether there is an appropriate dummy space in the active blocks which are dedicated to serve larger program data. For instance, to serve a program with size 4, active block 3 which serves a data size range of 5 to 8 is inspected first. If an appropriate dummy space could not be found there, it is served by active block 2 where the program is originally headed to, based on its size. However, only when the dummy space in the active blocks which are dedicated to serve larger data is determined to be ``unavailable" (since it is smaller than the smallest program data size the active block can serve), smaller programs are allowed to be placed into such space. For example, if the dummy space size in active block 3 is 5, a program whose size is 4 cannot use it, since it is still ``available" for the originally dedicated program size (5). Consequently, as shown in Figure \ref{ps-plus-io}, multiple active blocks can significantly reduce the dummy space. Even the remaining space generated by the active blocks serving larger ranges is consumed by smaller programs. The unavailable space produced by two programs with size 5 in active block 3 are spent by two programs with sizes of 2 and 3, which are originally served by active blocks 1 and 2, respectively. Algorithm \ref{alg:ps-plus} describes our new page allocation in detail.


%such program has to destine to the original active block, if the remaining space of the active blocks for bigger data sizes is enough to serve a future program whose size is in its specified range. 




















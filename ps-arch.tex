




\subsection{Architecture Overview}
The central design concept behind our new architecture is that the sectors that constitute a page are capable of being programmed separately (independent of one another). However, programming a sector independently should not hurt the effective data which is  previously written in other sectors of the page. To achieve this, the shared WL needs be redesigned to partially apply the very high voltage ($V_{pgm}$) to the specified portion. At the same time, $V_{pass}$ (the voltage applied to all other pages) needs to be applied to all of the remaining portions of the WL.

We incorporate a set of \emph{voltage lines} and \emph{switches} to the traditional flash architecture to enable sector-based programming without any reliability loss. Figure \ref{ps-archi} depicts a zoomed view which focuses on the first two sectors of three pages in a flash block. The voltage line (VL) is placed between any two sectors and attached to all WLs. A portion of the WL separated by the voltage line matches to the sector size, which is referred to as sector line (\emph{ScL}). Either $V_{pgm}$ or $V_{pass}$ can be assigned to each voltage line, depending on which sectors are supposed to be programmed. Furthermore, since the target WL and all the voltage lines are connected, we need a set of switches to regulate the current flow prevent conflicts. As shown in Figure \ref{ps-archi}, the switches are placed on the WL in both sides of each voltage line. Each switch determines whether the current originated from the neighbor voltage line flows on the ScL or not.

The detailed switch design we use in our architecture is also shown in the figure. The switches have a simple design that consists of one NMOS transistor and one PMOS transistor. The PMOS is connected to ground, while the NMOS is connected to the neighboring VL. The gates of both transistors are supplied with the control input ($V_{in}$) from the sector activation logic (SAL), which is a part of the flash control logic. The shared node between the two transistors is connected to the ScL. When the control input $V_{in}$ is zero, the ScL is connected to ground, and when $V_{in}$ is one, the ScL is supplied with the voltage from the VL.



With the support of the additional voltage lines and switches, PageSplit is able to apply the very high voltage ($V_{pgm}$) to only the specified ScL(s) and the low voltage ($V_{pass}$) to all the remaining ScL(s) in the target WL. Through this sector-based programming, the effective data (which is the data already programmed in specific sectors) will not get hurt by applying $V_{pass}$ to the corresponding ScL, when the other sectors in the same page are programmed. To program the target sectors (or ScLs) of the target page, a combination of voltage line values and switch on/off is determined by SAL.
%The change in the new architecture is still able to let all other WLs (pages) to be at ($V_{pass}$) as the conventional architecture does.

\begin{figure}
\centering
\includegraphics[scale=.22]{ps-arch}
\vspace{-5pt}
\caption{Our PageSplit architecture includes the voltage line and the switches, which separate two neighboring sectors within a page.}
\label{ps-archi}
\vspace{-5pt}
\end{figure}




\begin{figure}[t]
\centering
\vspace{-5pt}
\subfloat[Program of Sector 1.]{\label{fig:arch-exam1}\rotatebox{0}{\includegraphics 
[width=0.85\linewidth] {ps-exam1}}}   
%\newline
\hspace{2pt}
\subfloat[Program of Sectors 2 and 3.]{\label{fig:arch-exam2}\rotatebox{0}{\includegraphics 
[width=0.98\linewidth]  {ps-exam2}}}
\vspace{-5pt}
\caption{Two examples of sector-based programs enabled by PageSplit.}
\vspace{-15pt}
\label{fig:archi-exam}
\end{figure}



\subsection{Sector Program Examples}
This section goes over two examples illustrating how the SAL of the flash control logic determines the voltage line values and switch on/off to program the specified sectors. To indicate each voltage line and switch, we need to name the circuits; in a n-sector page flash, the voltage lines are numbered from VL1 to VL\emph{n}, from the left to the right. For a switch, its name is expressed as (WL\#, VL\#, L/R), since the switches in each WL are placed on the left and right sides of each voltage line.


Figure \ref{fig:arch-exam1} illustrates a sector program (where the target sector is Sector 1 of $WL_{N}$). To apply $V_{pgm}$ to only ScL1 of $WL_{N}$, $V_{pass}$ is given to all the VLs for all other ScLs of $WL_{N}$ and all other WLs. On the other hand, the high voltage for ScL1 of $WL_{N}$ is provided by the original WL source as it would be done in a conventional architecture. By turning off Switch(N, 1, L), the other end of the target ScL is grounded and disconnected from VL1. For all other ScLs (ScL2 to ScL8 of $WL_{N}$ and all ScLs of other WLs), the left-side switches of each VL are tuned off, while the right-side switches are turned on, which makes the current of $V_{pass}$ (provided by each VL) flow.


On the other hand, Figure \ref{fig:arch-exam2} depicts the case of programming both Sectors 2 and 3 of $WL_{N}$. The high voltage $V_{pgm}$ has to be applied to only ScL2 and ScL3 of $WL_{N}$, while all the other ScLs of $WL_{N}$ and all of other WLs) are at $V_{pass}$. To implement this, $V_{pgm}$ is given to VL2, while $V_{pass}$ is applied to VL1 and VL3. To make the current of VL2 flow on ScL2 and ScL3, both Switch(N, 2, L) and Switch(N, 2, R) are turned on, and Switch(N, 1, R) and Switch (N, 3, L) on both ends are grounded. Switch(N, 1, R) and Switch (N, 3, L) also function to disconnect the target sectors from VL1 and VL3 (which are at $V_{pass}$). All the other switches around VL2 (i.e., Switch(N-1, 2, L), Switch(N-1, 2, R), Switch(N+1, 2, L), and Switch(N+1, 2, R)) should be turned off to prevent the high voltage current from flowing on other ScLs. Similar to the first example, all other ScLs are given $V_{pass}$ by turning on/off switches. As described in both these examples, \emph{PageSplit allows any sector(s) in a page to be programmed without repeatedly applying the high voltage to the whole WL}.








\subsection{Reuse of Dummy Space}
\label{sec:archi-io}
Any sector can be programmed independently from the programs of other sectors in the same page, since PageSplit can successfully block the intra-page program disturb. This critical feature helps us address the dummy space problem caused by misaligned I/O requests. A new program can place its data into \emph{any} dummy space whose size is equal to or larger than the data size. In this way, PageSplit can eliminate flash space wastage by making entire page fully occupied with user I/O data. 

An important constraint, however, could limit this ideal use (which is to serve a program command to any dummy space in the flash) of PageSplit: \emph{in-page-order programming} \cite{page-order}\cite{program-distrub-1}. This constraint requires a given set of program commands to be served in the flash pages according to the pre-defined page sequence within a block. The consumption of a physical page by the specified page order is enforced in order to minimize the effect of the \emph{inter-page program disturb}. This type of program disturb indicates the data corruption of the neighboring pages caused by the target page program \cite{program-distrub-1}\cite{program-distrub-2}\cite{program-distrub-3}. Due to the in-page-order programming constraint, within a block, the dummy space in page N is not allowed to be used after the pages whose number is N+1 or higher are programmed. Therefore, when a new page is allocated to serve the current program and its data size is smaller than the page size (once we get a dummy space in the current page), the remaining space has to be spent by serving subsequent commands before allocating a new page, since it \emph{cannot} be used later.

Figure \ref{ps-io} provides an example where two architectures serve a series of program commands in a block. The conventional architecture (on the left) quickly consumes the new pages by generating dummy places in each page, whereas PageSplit (on the right) reduces the page consumption rate by re-using the remaining space produced by the previous programs in a page. As a result of serving the six shown program commands, PageSplit can save two new pages by reducing the dummy place, compared to the conventional architecture. Note also that \emph{PageSplit also complies with the program order in a block}. We can conclude that the proposed architecture can reduce the space wastage brought by I/O misalignment without any reliability loss by considering both the intra-page and inter-page program disturb.


\begin{figure}
\centering
\includegraphics[scale=.25]{ps-io}
\vspace{-5pt}
\caption{The page consumption scenarios in two architectures. Compared to the conventional one, PageSplit can save new pages by reusing the dummy space.}
\label{ps-io}
\vspace{-20pt}
\end{figure}


\subsection{Flash Software Support}
\label{sec:ftl}
This new flash architecture brings a slight modification to the flash software. The required changes are twofold: \emph{page allocation} and \emph{address mapping}.

\noindent \emph{\bf Page Allocation:~} An important functionality of the flash software is to allocate a new physical page to serve the current given program command. Based on the specified program order (to minimize the inter-page program disturb), the flash software allocates a page (called \emph{active page}) of the current block (called \emph{active block}), one by one, for the coming program data. Once all the pages in an active block are used up, a new active block is allocated and the first page (in program order) of the block becomes a new active page for the next program service. However, PageSplit needs a new page allocation strategy, unlike the default page allocation which provides a new active page for every program command. As described in Figure \ref{ps-io}, PageSplit's page allocation considers the remaining space of the active page and the current program data size. If the program data size is equal to or smaller than the free space size in the active page, the space is used for the program without allocating a new page. However, if the remaining page space is not big enough to serve the current program size, a new active page is allocated to serve it. In such a case, the free space in the previous page has to remain dummy, since a revisit to the space after using neighboring pages violates the in-page-order programming constraint.


\noindent \emph{\bf Page Mapping:~} Since any logical page can be placed in any physical page of the flash chip, the flash software implements a mapping mechanism between LPN and PPN. Originally, 
an entry of the address mapping table consists of  PPN, page-offset, and page-size, which is indexed by LPN. PPN indicates the physical page where the logical page resides, and both page-offset and page-size specify the effective sector portion of the logical page. In comparison, PageSplit allows the data segments of two different logical pages to reside in the same physical page. Theoretically, different logical page segments (as many as the number of sectors in the page) can share the single physical page space. To distinguish among the different logical page data residing in the same page, additional information that captures the position of each logical page data in a page should be included in the address mapping table entry. Since the minimum program unit in PageSplit is a \emph{sector}, a set of bits expressing the sector positions in each page are appended to the table entry. For example, if a logical page data segment whose size is three is programmed in the bit positions 3 to 5 in an 8-sector physical page, the corresponding entry has a series of bits, which are encoded as (0, 0, 1, 1, 1, 0, 0, 0). With the data segment in the center of the page, the two sectors on the left and the three sectors on the right might be used by other logical page segments.


\noindent \emph{\bf Command Format:~} The NAND command used in PageSplit has to additionally specify the sector position in the target page. To construct a program command, the page allocator first returns both the active page and the start position of the remaining space in the page. After updating the address mapping table with the physical page information including the sector positions, the command is delivered to the target flash chip, in the form of (block\#, page \#, sector \#).


\subsection{Hardware Overhead} PageSplit exploits two types of circuits to realize sector-based programming: voltage lines and switches. Voltage lines are physically the same as WLs, which are metal lines to flow the current based on the applied voltage. Since their purpose is to divide a page into sectors, the number of voltage lines ($N_{VL}$) is the same as the number of sectors in a page. A set of voltage lines is shared by all pages in a block. On the other hand, switches are placed on WLs and located in both sides of each voltage line. For the last voltage line, only one switch is put on the left of it, which gives us the total number of switches for a page as ($N_{VL}\times2-1$). As the page size increases, the number of voltage lines and switches to implement PageSplit also increases. However, the actual space they take is not a big overhead when considering a huge amount of FGTs consisting of the memory arrays.

















